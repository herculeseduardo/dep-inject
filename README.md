## Awilix dependency injection

# Install

npm install

# Test api

```
GET http://localhost:3001/api/user

GET http://localhost:3001/api/customer

POST http://localhost:3001/api/user
{
	"name": "João Silva",
	"email": "joao@teste.com"
}

POST http://localhost:3001/api/customer
{
	"name": "Domingos Freitas",
	"address": "av. Itamaraty, 54"
}
```
