const express = require('express')
const router = express.Router()
const { cradle } = require('./di')

router.use('/api/user', require('./modules/user')(cradle))
router.use('/api/customer', require('./modules/customer')(cradle))

module.exports = router
