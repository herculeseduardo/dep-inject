const mongoose = require('mongoose')
mongoose.set('debug', true)

const getMongoConnectionByURI = ({ uri }) => {
  return mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
}

module.exports = {
  getMongoConnectionByURI
}
