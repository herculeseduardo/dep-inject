require('dotenv/config')
const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = process.env.APP_PORT || 3000
const { getMongoConnectionByURI } = require('./mongo')
const logger = require('./utils/logger')
const routes = require('./routes')

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(bodyParser.text())

getMongoConnectionByURI({ uri: process.env.MONGO_URI })
  .then(connection => {
    logger.info(`MongoDB connected!`)
  })
  .catch(console.error)

logger.stream = {
  write: function (message, encoding) {
    logger.debug(message)
  }
}
app.use(require('morgan')('tiny', { stream: logger.stream }))

app.use(routes)

app.use((req, res, next) => {
  const err = new Error('NOT_FOUND')
  err.status = 404
  err.code = 'ERR_NOT_FOUND'
  next(err)
})

app.use((err, req, res, next) => {
  res.status(err.status || 500)
  if (!err.code) {
    console.trace(err)
  }
  res.json(err)
})

app.listen(port, () => {
  logger.info(`Server running at http://localhost:${port}/`)
})
