const userService = ({ logger, userRepository /* , customerService */ }) => ({
  save: async ({ name, email }) => {
    logger.info(`customer service`)
    await userRepository.saveUser({ name, email })
  },
  get: async () => {
    const users = await userRepository.getUsers()
    return users
  }
})

module.exports = {
  userService
}
