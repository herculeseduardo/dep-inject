const faker = require('faker')
const { User } = require('../../../models/user')

const userRepository = ({ logger }) => {
  return {
    saveUser: async ({ name, email }) => {
      try {
        await User.create({ name, email })
      } catch (err) {
        throw err
      }
    },
    getUsers: async () => {
      const user = faker.name.findName()
      logger.info(`userRepository getUser: ${user}`)
      const users = await User.find()
      return users
    }
  }
}

module.exports = {
  userRepository
}
