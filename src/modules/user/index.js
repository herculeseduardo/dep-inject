const { Router } = require('express')
const router = new Router()

module.exports = ({ userService, logger /* , customerService */ }) => {
  router.get('/', async (req, res, next) => {
    try {
      logger.info('get users')
      const items = await userService.get()
      res.status(200).json(
        items.map(item => ({
          name: item.name,
          email: item.email
        }))
      )
    } catch (err) {
      return next(err)
    }
  })

  router.post('/', async (req, res, next) => {
    try {
      logger.info('creating user')
      const { name, email } = req.body
      const ret = await userService.save({
        name,
        email
      })
      res.status(201).json({
        ok: true,
        message: 'User created'
      })
    } catch (err) {
      return next(err)
    }
  })

  return router
}
