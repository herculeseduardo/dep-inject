const { Customer } = require('../../../models/customer')

const customerRepository = ({ logger }) => {
  return {
    saveCustomer: async ({ name, address }) => {
      try {
        await Customer.create({ name, address })
      } catch (err) {
        throw err
      }
    },
    getCustomers: async () => {
      const customers = await Customer.find()
      logger.info(`customerRepository getCustomers`)
      return customers
    }
  }
}

module.exports = {
  customerRepository
}
