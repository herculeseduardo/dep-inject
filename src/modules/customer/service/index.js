const customerService = ({ customerRepository }) => {
  return {
    saveCustomerService: async ({ name, address }) => {
      await customerRepository.saveCustomer({ name, address })
    },
    getCustomerService: async () => {
      const customers = await customerRepository.getCustomers()
      return customers
    }
  }
}

module.exports = {
  customerService
}
