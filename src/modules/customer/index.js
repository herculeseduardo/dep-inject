const { Router } = require('express')
const router = new Router()

module.exports = ({ logger, customerService }) => {
  router.get('/', async (req, res, next) => {
    try {
      logger.info('get customers')
      const items = await customerService.getCustomerService()
      res.status(200).json(
        items.map(item => ({
          name: item.name,
          address: item.address
        }))
      )
    } catch (err) {
      return next(err)
    }
  })

  router.post('/', async (req, res, next) => {
    logger.info('creating customer')
    const { name, address } = req.body
    await customerService.saveCustomerService({
      name,
      address
    })
    res.status(201).json({
      ok: true,
      message: 'User created'
    })
  })

  return router
}
