const { createContainer } = require('awilix')

/* modules */
const repository = require('./repositories')
const service = require('./services')
const logger = require('./logger')

const container = createContainer()

container.register(repository)
container.register(service)
container.register(logger)

module.exports = container
module.exports.cradle = container.cradle
