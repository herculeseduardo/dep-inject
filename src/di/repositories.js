/* asClass, asFunction, asValue */
const { asFunction } = require('awilix')

const { userRepository } = require('../modules/user/repository')
const { customerRepository } = require('../modules/customer/repository')

module.exports = {
  userRepository: asFunction(userRepository),
  customerRepository: asFunction(customerRepository)
}
