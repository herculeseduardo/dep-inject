/* asClass, asFunction, asValue */
const { asValue } = require('awilix')

const logger = require('../utils/logger')

module.exports = {
  logger: asValue(logger)
}
