/* asClass, asFunction, asValue */
const { asFunction } = require('awilix')

const { userService } = require('../modules/user/service')
const { customerService } = require('../modules/customer/service')

module.exports = {
  userService: asFunction(userService),
  customerService: asFunction(customerService)
}
